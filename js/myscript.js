var app = angular.module('myApp', []);

app.controller('ctrl',['$scope','$http',
    function($scope,$http) {
        $scope.pop = false;
        $scope.error_api = "";
        $scope.signedup = false;
        $scope.signup_error = false ;
        $scope.login_error = false ;

        $scope.error = false;
        $scope.error_message = 'undefined';
        $scope.user = {
            'username' : 'undefined' ,
            'password' : 'undefined',
            'first_name' : 'undefiend',
            'authenticated' : false
        };

        $scope.togglePop = function() {
            $scope.pop = !$scope.pop;
        }

        $scope.logout = function() {
            $scope.user.authenticated = false ;
            $scope.user.first_name = 'undefiend';
            $scope.user.password = 'undefined';
            $scope.user.username = 'undefined';
        }

        $scope.sharedGps = function() {
            var username = $scope.user.username;
            console.log(username);
            $http({
                method: 'HEAD',
                url: 'https://tubair-tubair.rhcloud.com/gps/' + username
                }).then(function successCallback(response) {
                    console.log(response);
                    console.log("shared");
                    $scope.hasShared = true;
                }, function errorCallback(response) {
                    console.log(response);
                    console.log("not shared");
                    $scope.hasShared = false;
            })
        }

        $scope.login = function(){
            $http({
                method: 'HEAD',
                url: 'https://tubair-tubair.rhcloud.com/users/tubairusermng16v1.1qntom/' + $scope.username_log + '/' + $scope.password_log
            }).then(function successCallback(response) {
                if(response.status == '200'){
                    $scope.user.first_name = $scope.username_log;
                    $scope.user.username = $scope.username_log;
                    $scope.user.authenticated = true ;
                    $scope.sharedGps();
                }else{
                    $scope.user.authenticated = false ;
                    $scope.error = true;
                    $scope.error_message = 'Identifiant ou mot de passe incorrect';
                }
            }, function errorCallback(response) {
                $scope.error = true;
                $scope.error_message = 'Identifiant ou mot de passe incorrect';
            });
        }
        $scope.signup = function() {
            $http({
                method: 'POST',
                url: 'https://tubair-tubair.rhcloud.com/users/tubairusermng16v1.1qntom/' + $scope.email + '/' + $scope.password
            }).then(function successCallback(response) {
                if(response.status == '200'){
                    $scope.signedup = true ;
                    $scope.signup_error = false ;
                }else{
                    $scope.signedup = false;
                    $scope.signup_error = true ;
                }
            }, function errorCallback(response) {
                $scope.signup_error = true ;
            });

        }

        $scope.showPopup = true;

        $scope.togglePopup = function() {
            $scope.showPopup = !$scope.showPopup;
        }

        var mysrclat= 0; var mysrclong = 0;

        $scope.canSeeMeasures = function() {
            var username = $scope.user.username;
            $http({
                method: 'HEAD',
                url: 'https://tubair-tubair.rhcloud.com/gps/' + username
                }).then(function successCallback(response) {
                    var newUrl = 'graphiques.html?user=' + username;
                    window.location.replace(newUrl);
                }, function errorCallback(response) {
                    //
                    var newUrl = 'graphiques.html?user=' + username;
                    window.location.replace(newUrl);
                    //
                    //alert("Vous devez partager vos données de géolocalisation pour voir vos données, rendez-vous plus bas sur la page !");
            })
        }

        $scope.nearme = function() {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    mysrclat = position.coords. latitude;
                    mysrclong = position.coords.longitude;
                    var username = $scope.user.username;
                    $http({
                        method: 'HEAD',
                        url: 'https://tubair-tubair.rhcloud.com/gps/' + username
                    }).then(function successCallback(response) {

                        alert("Votre position est déjà enregistrée dans nos données");
                    }, function errorCallback(response) {
                        if (response.status == 404) {
                            $http({
                                method: 'POST',
                                url: 'https://tubair-tubair.rhcloud.com/gps/' + username + '/' + mysrclong + '/' + mysrclat
                            }).then(function successCallback(response) {
                                alert("Vos données ont bien été envoyées");
                            }, function errorCallback(response) {
                                alert("Vos données n'ont pas pu être envoyées");
                            });
                        } else {
                            alert("Votre position est déjà enregistrée dans nos données");
                        }
                    });
                });
            }
        }

        $scope.gpsDelete = function() {
            var username = $scope.user.username;
            $http({
                method: 'DELETE',
                url: 'https://tubair-tubair.rhcloud.com/gps/tubairusermng16v1.1qntom/' + username
            }).then(function successCallback(response) {
                alert("Vos données ne sont désormais plus partagées");
            }, function errorCallback(response) {
                alert("Erreur lors de la suppression de vos données GPS");
            });
        }
    }
]);

app.directive("myMaps", function($http) {
    var getAllMarkers = function (map,infowindow) {
        var markerImage;
        $http({
            method: 'GET',
            url: 'https://tubair-tubair.rhcloud.com/gps/' + 'tomquentinxtuba16accessgpspwconnect16'
        }).then(function successCallback(response) {
            var counter = 0;
            var marker ;
            while (counter < response.data.length) {
                var number1 = parseFloat(response.data[counter].lat);
                var number2 = parseFloat(response.data[counter].longitude);
                var usernamecurrent = response.data[counter].user;
                var myCoordinates = new google.maps.LatLng(number1, number2);
                marker = new google.maps.Marker({
                    position : myCoordinates,
                    map : map
                });
                google.maps.event.addListener(marker,'click',(function(marker,usernamecurrent){
                    return function() {
                        infowindow.setContent("<a>"+ usernamecurrent +"</a><br><a href=graphiques.html?user=" + usernamecurrent + ">Voir les mesures de cet utilisateur !</a>");
                        infowindow.open(map,marker);
                    }
                })(marker,usernamecurrent));
                counter ++;
            }
        }, function errorCallback(response) {
            getAllMarkers(map,infowindow);
        });
    }
    return {
        restrict:'E',
        template:'<div></div>',
        replace:true,
        link:function(scope, element, attrs) {
            var myLatLng = new google.maps.LatLng(45.760053, 4.85857);
            var mapOptions = {
                center: myLatLng,
                zoom: 3,
                mapTypeId: google.maps.MapTypeId.SATELLITE
            };
            var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
            var infowindow = new google.maps.InfoWindow();
            getAllMarkers(map,infowindow);
        }
    };
});