var app = angular.module("app", ["chart.js"]);

    app.controller("LineCtrl", ['$scope','$http', function($scope,$http) {

        $scope.username = window.location.href.split("?")[1].split("=")[1];
        $scope.period = "jour";
        $scope.channel = 'T';
        $scope.add = false;
        $scope.noData = false;
        $scope.lastIndex = 0;
        $scope.changeIndex = false;
        $scope.allUsers = [];
        $scope.isUserDisplayed = [];
        $scope.allUsersDisplayed = [];
        $scope.countAdded = 0;
        $scope.alreadyAdded = 0;
        $scope.multipleAdd = false;
        $scope.currentDate = "";
        $scope.currentMonth = "";
        $scope.currentYear = "";

        $scope.colors = ['#FE1B00', '#FFFFFF', '#47EDC9', '#A634AE', '#EDBD23', '#1596D4', '#23333B'];
        $scope.colorsBis = ['#FFFFFF', '#47EDC9', '#A634AE', '#EDBD23', '#1596D4', '#23333B', '#FE1B00'];
        $scope.userColors = ['#65C3AB', '#65C3AB', '#65C3AB', '#65C3AB', '#65C3AB', '#65C3AB', '#65C3AB'];

        $scope.data_tmp = [];
        $scope.labels_tmp = [];
        $scope.series_tmp = [];
        $scope.data_values = [];
        $scope.data_avg = [];
        $scope.data_min = [];
        $scope.data_max = [];
        $scope.max_label = 0;
        $scope.nbr_mesure = 0;
        $scope.last_mesure = 0;
        $scope.indicator_tab_tmp = [];
        $scope.last_tmp = "--";
        $scope.avg_tmp = 0;

        //sélecteur de jour
        jQuery(function($) {
            $('#datepicker').datepicker({
                inline:true,
                firstDay: 1,
                maxDate: 0,
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                showOtherMonths: true,
                altField: "#datepicker",
                altFormat: "dd-mm-yy",
                dateFormat: "dd-mm-yy",
                onSelect: function(date) {
                    $('#date-start-output').html(date);
                    $scope.currentDate = date;
                    $scope.data_tmp = [];
                    $scope.labels_tmp = [];
                    $scope.series_tmp = [];
                    $scope.data_min = [];
                    $scope.data_max = [];
                    $scope.data_values = [];
                    $scope.data_avg = [];
                    $scope.redirection($scope.username);
                }
            });
        });

        // REQUESTS
        $scope.requestForUsers = function() {
            $http({
                method: 'GET',
                url: 'https://tubair-tubair.rhcloud.com/gps/' + 'tomquentinxtuba16accessgpspwconnect16'
            }).then(function successCallback(response) {
                var i = 0;
                while (i < response.data.length) {
                    if (response.data[i].user != $scope.username) {
                        $scope.allUsers.push(response.data[i].user);
                        $scope.isUserDisplayed.push(false);
                    }
                    i++;
                }
            }, function errorCallback(response) {
            });
        };

        $scope.requestForAllValues = function(user, callback) {
            $http({
                method: 'GET',
                url: 'https://tubair-tubair.rhcloud.com/mesures/tuba1/' + user
            }).then(function successCallback(response) {
                return callback(response.data);
            }, function errorCallback(response) {
            });
        };

        $scope.requestForAverages = function(username, callback) {
            $http({
                method: 'GET',
                url: 'https://tubair-tubair.rhcloud.com/mesures/tuba1/avg/' + username + '/' + $scope.channel + '/' + $scope.period + '/' + $scope.currentDate
            }).then(function successCallback(response) {
                return callback(response.data, username);
            }, function errorCallback(response) {
            });
        };

        //DATE AND CHANNEL CHANGES
        $scope.changeDate = function(periodToChange) {
            if (periodToChange === 'month') {
                $scope.currentDate = $scope.currentDate.split('-')[0] + '-' + $scope.currentMonth + '-' + $scope.currentDate.split('-')[2]; 
            } else if (periodToChange === 'year') {
                $scope.currentDate = $scope.currentDate.split('-')[0] + '-' + $scope.currentDate.split('-')[1] + '-' + $scope.currentYear;
            }
            $scope.data_tmp = [];
            $scope.labels_tmp = [];
            $scope.series_tmp = [];
            $scope.data_min = [];
            $scope.data_max = [];
            $scope.data_values = [];
            $scope.data_avg = [];
            $scope.redirection();
        };

        changeMonth = function(newMonth) {
            var i = 0;
            var allMonthes = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
            var allNbrMonthes = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
            while (i < allMonthes.length) {
                if (newMonth === allMonthes[i]) {
                    $scope.currentMonth = allNbrMonthes[i];
                }
                i++;
            }
            $scope.changeDate('month');
        };

        changeYear = function(newYear) {
            var i = 0;
            var allYears = ['2016'];
            while (i < allYears.length) {
                if (newYear === allYears[i]) {
                    $scope.currentYear = allYears[i];
                }
                i++;
            }
            $scope.changeDate('year');
        };

        $scope.changeChannel = function(data) {
            $scope.channel = data;
            if ($scope.period != 'jour') {
                $scope.data_min = [];
                $scope.data_max = [];
                $scope.data_values = [];
                $scope.data_avg = [];
            }
            if ($scope.countAdded != 0) {
                $scope.multipleAdd = true;
            }
            $scope.allUsersDisplayed = [];
            $scope.data_tmp = [];
            $scope.labels_tmp = [];
            $scope.series_tmp = [];
            $scope.indicator_tab_tmp = [];
            $scope.add = false;
            $scope.redirection($scope.username);
        };

        $scope.changePeriod = function(period) {
            $scope.period = period;
            if ($scope.period != 'jour') {
                $scope.data_min = [];
                $scope.data_max = [];
                $scope.data_values = [];
                $scope.data_avg = [];
            }
            if ($scope.countAdded != 0) {
                $scope.multipleAdd = true;
            }
            $scope.allUsersDisplayed = [];
            $scope.data_tmp = [];
            $scope.labels_tmp = [];
            $scope.series_tmp = [];
            $scope.indicator_tab_tmp = [];
            $scope.add = false;
            $scope.redirection($scope.username);
        };

        //ADD USER
        $scope.isAlreadyAdded = function() {
            var i = 0;
            while (i < $scope.allUsersDisplayed.length) {
                if ($scope.allUsersDisplayed[i] == $scope.userToAdd) {
                    return (i);
                }
                i++;
            }
            return (- 1);
        };

        $scope.addUser = function(otheruser, index) {
            $scope.lastIndex = index;
            $scope.userToAdd = otheruser;
            $scope.add = true;
            $scope.changeIndex = true;
            $scope.data_avg = [];
            $scope.alreadyAdded = $scope.isAlreadyAdded();
            $scope.isUserDisplayed[$scope.lastIndex] = !$scope.isUserDisplayed[$scope.lastIndex];
            if ($scope.isUserDisplayed[$scope.lastIndex] == true) {
                $scope.userColors[$scope.lastIndex] = $scope.colors[$scope.data_tmp.length];
            } else {
                $scope.userColors[$scope.lastIndex] = '#65C3AB';
            }
            $scope.redirection($scope.userToAdd);
        };

        $scope.adjustColors = function() {
            var i = 0;
            var j;
            while (i < $scope.allUsers.length) {
                j = 0;
                while (j < $scope.allUsersDisplayed.length) {
                    if ($scope.allUsers[i] === $scope.allUsersDisplayed[j]) {
                        if ($scope.isUserDisplayed[i] == true) {
                            $scope.userColors[i] = $scope.colors[$scope.data_tmp.length - 1];
                        } else {
                            $scope.userColors[i] = '#65C3AB';
                        }
                    }
                    j++;
                }
                i++;
            }
        };

        $scope.pushAddedUser = function(Data, username) {
            Data = $scope.lissage(Data);
            $scope.data_tmp.push(Data);
            $scope.series_tmp.push(username);
        };

        //PUSH
        $scope.pushSeries = function() {
            if ($scope.period === 'jour') {
                if ($scope.channel === 'T') {
                    $scope.series_tmp.push('Moyenne conseillée', $scope.username);
                } else if ($scope.channel === 'H') {
                    $scope.series_tmp.push('Humidité maximale', $scope.username);
                } else if ($scope.channel === 'CO') {
                    $scope.series_tmp.push('Seuil extrême', $scope.username);
                } else {
                    $scope.series_tmp.push($scope.username);
                }
            } else {
                if ($scope.add == true) {
                    if ($scope.alreadyAdded == - 1) {
                        $scope.series_tmp.push($scope.userToAdd);
                    } else if ($scope.alreadyAdded >= 0) {
                        $scope.series_tmp.splice($scope.alreadyAdded, 1);
                    }
                } else {
                    if ($scope.channel === 'T') {
                        $scope.series_tmp.push('Moyenne conseillée', $scope.username, 'Valeurs minimales', 'Valeurs maximales');
                    } else if ($scope.channel === 'H') {
                        $scope.series_tmp.push('Humidité maximale', $scope.username, 'Valeurs minimales', 'Valeurs maximales');
                    } else if ($scope.channel === 'CO') {
                        $scope.series_tmp.push('Seuil extrême', $scope.username, 'Valeurs minimales', 'Valeurs maximales');
                    } else {
                        $scope.series_tmp.push($scope.username, 'Valeurs minimales', 'Valeurs maximales');
                    }
                }
            }
        };

        $scope.pushData = function(Data) {
            if ($scope.period === 'jour') {
                if ($scope.channel === 'T') {
                    $scope.data_tmp.push($scope.indicator_tab_tmp, Data);
                } else if ($scope.channel === 'H') {
                    $scope.data_tmp.push($scope.indicator_tab_tmp, Data);
                } else if ($scope.channel === 'CO') {
                    $scope.data_tmp.push($scope.indicator_tab_tmp, Data);
                } else {
                    $scope.data_tmp.push(Data);
                }
            } else {
                if ($scope.add == true) {
                    if ($scope.alreadyAdded == - 1) {
                        $scope.countAdded++;
                        $scope.data_tmp.push(Data);
                        $scope.allUsersDisplayed.push($scope.userToAdd);
                    } else if ($scope.alreadyAdded >= 0) {
                        $scope.countAdded--;
                        if ($scope.countAdded == 0) {
                            $scope.multipleAdd = false;
                        }
                        $scope.data_tmp.splice($scope.alreadyAdded, 1);
                        $scope.allUsersDisplayed.splice($scope.alreadyAdded, 1);
                        $scope.adjustColors();
                    }
                } else {
                    if ($scope.channel === 'T') {
                        $scope.data_tmp.push($scope.indicator_tab_tmp, Data, $scope.data_min, $scope.data_max);
                    } else if ($scope.channel === 'H') {
                        $scope.data_tmp.push($scope.indicator_tab_tmp, Data, $scope.data_min, $scope.data_max);
                    } else if ($scope.channel === 'CO') {
                        $scope.data_tmp.push($scope.indicator_tab_tmp, Data, $scope.data_min, $scope.data_max);
                    } else {
                        $scope.data_tmp.push(Data, $scope.data_min, $scope.data_max);
                    }
                    if ($scope.channel === 'T' || $scope.channel === 'H' || $scope.channel === 'CO') {
                        $scope.allUsersDisplayed.push('indicator');
                    }
                    $scope.allUsersDisplayed.push($scope.username, 'min', 'max');
                }
                var i = 0;
                if ($scope.multipleAdd == true) {
                    while (i < $scope.allUsers.length) {
                        if ($scope.isUserDisplayed[i] == true) {
                            $scope.data_avg = [];
                            $scope.userToAdd = $scope.allUsers[i];
                            $scope.add = true;
                            $scope.requestForAverages($scope.userToAdd, $scope.pushAddedUser);
                        }
                        i++;
                    }
                    $scope.multipleAdd = false;
                }
            }
        };

        $scope.lissage = function(data_tab) {
            var i = 0;
            var prec_value;
            var next_value;
            var counter;
            while (i < data_tab.length) {
                data_tab[i] = Math.round(data_tab[i]*100)/100;
                if (data_tab[i] != 0) {
                    if (i + 1 < data_tab.length) {
                        if (data_tab[i + 1] == 0) {
                            prec_value = data_tab[i];
                            counter = i + 1;
                            next_value = 0;
                            while (counter < data_tab.length && next_value == 0) {
                                if (data_tab[counter] != 0) {
                                    next_value = Math.round(data_tab[counter]*100)/100;
                                }
                                counter++;
                            }
                            if (data_tab[i + 1] == 0 && next_value != 0) {
                                data_tab[i + 1] = (prec_value + next_value) / 2;
                            } else if (data_tab[i + 1] == 0 && next_value == 0) {
                                data_tab.splice(i + 1, data_tab.length - i);
                            }
                        }
                    }
                }
                i++;
            }
            return (data_tab);
        };

        $scope.fillIndicatorAndPush = function() {
            var i = 0;
            var tmp_tab = [];
            if ($scope.period === 'jour') {
                if ($scope.data_values.length == 0) {
                    $scope.noData = true;
                }
                while (i < $scope.data_values.length) {
                    tmp_tab[i] = $scope.data_values[i].valeur;
                    $scope.labels_tmp.push((parseInt($scope.data_values[i].date.split('T')[1].split(':')[0]) - 1).toString() + ':' + $scope.data_values[i].date.split('T')[1].split(':')[1]);
                    i++;
                }
            } else {
                if ($scope.data_avg.length == 0) {
                    $scope.noData = true;
                }
                i = 1;
                while (i <= $scope.data_avg.length) {
                    tmp_tab[i] = $scope.data_avg[i];
                    if ($scope.add == false) {
                        if ($scope.period === 'mois') {
                            $scope.labels_tmp.push(i + '/' + $scope.currentMonth);
                        } else if ($scope.period === 'annee') {
                            $scope.labels_tmp.push(i + '/' + $scope.currentYear);
                        }
                    }
                    i++;
                }
            }
            tmp_tab = $scope.lissage(tmp_tab);
            i = 0;
            while (i < tmp_tab.length) {
                if ($scope.channel === 'T') {
                    $scope.indicator_tab_tmp.push(20);
                } else if ($scope.channel === 'H') {
                    $scope.indicator_tab_tmp.push(70);
                } else if ($scope.channel === 'CO') {
                    $scope.indicator_tab_tmp.push(20);
                }
                i++;
            }
            $scope.pushSeries();
            $scope.pushData(tmp_tab);
        };

        //SORT
        $scope.channelSort = function(allDatas, callback) {
            var i = 0;
            var channelSorted_tab = [];
            while (i < allDatas.length) {
                if (allDatas[i].polluant === $scope.channel) {
                    channelSorted_tab.push(allDatas[i]);
                }
                i++;
            }
            if ($scope.period === 'jour') {
                $scope.last_tmp = channelSorted_tab[channelSorted_tab.length - 1].valeur;
            }
            callback(channelSorted_tab);
        };

        $scope.dateSort = function(channelSorted_tab) {
            var i = 0;
            var dateSorted_tab = [];
            while (i < channelSorted_tab.length) {
                if (channelSorted_tab[i].date.split('T')[0].split('-')[0] === $scope.currentDate.split('-')[2]) {
                    if (channelSorted_tab[i].date.split('T')[0].split('-')[1] === $scope.currentDate.split('-')[1]) {
                        if (channelSorted_tab[i].date.split('T')[0].split('-')[2] === $scope.currentDate.split('-')[0]) {
                            dateSorted_tab.push(channelSorted_tab[i]);
                        }
                    }
                }
                i++;
            }
            $scope.data_values = dateSorted_tab;
            $scope.nbr_mesure = dateSorted_tab.length;
            if (dateSorted_tab.length == 0) {
                $scope.avg_tmp = "--";
            } else {
                $scope.avg_tmp = Math.round($scope.makeAverage(dateSorted_tab) * 100) / 100;
            }
            $scope.fillIndicatorAndPush();
        };

        $scope.sortValues = function(allDatas) {
            $scope.channelSort(allDatas, $scope.dateSort);
        };

        $scope.fillMinAndMax = function(tmp_tab) {
            var i = 0;
            var min = tmp_tab[0];
            var max = tmp_tab[0];
            while (i < tmp_tab.length) {
                if (tmp_tab[i] < min) {
                    min = tmp_tab[i];
                } else if (tmp_tab[i] > max) {
                    max = tmp_tab[i];
                }
                i++;
            }
            $scope.data_max.push(max);
            $scope.data_min.push(min);
        };

        $scope.makeAverage = function(tmp_tab) {
            var i = 0;
            var sum = 0;
            if ($scope.period != 'jour') {
                while (i < tmp_tab.length) {
                    sum = sum + tmp_tab[i];
                    i++;
                }
            } else {
                while (i < tmp_tab.length) {
                    sum = sum + tmp_tab[i].valeur;
                    i++;
                }
            }
            return (sum / tmp_tab.length);
        };

        $scope.makeAveragesMinMax = function(channelSorted_tab) {
            var i = 1;
            var j;
            var tmp_tab = [];
            var total_tab = [];
            if ($scope.period === 'mois') {
                $scope.max_label = 31;
            } else {
                $scope.max_label = 12;
            }
            while (i <= $scope.max_label) {
                j = 0;
                tmp_tab = [];
                if ($scope.period === 'mois') {
                    while (j < channelSorted_tab.length) {
                        if (parseInt(channelSorted_tab[j].jour) == i && channelSorted_tab[j].mois === $scope.currentMonth && channelSorted_tab[j].annee === $scope.currentYear) {
                            tmp_tab.push(channelSorted_tab[j].valeur);
                        }
                        j++;
                    }
                } else if ($scope.period === 'annee') {
                    while (j < channelSorted_tab.length) {
                        if (parseInt(channelSorted_tab[j].mois) == i && channelSorted_tab[j].annee === $scope.currentYear) {
                            tmp_tab.push(channelSorted_tab[j].valeur);
                        }
                        j++;
                    }
                }
                j = 0;
                while (j < tmp_tab.length) {
                    total_tab.push(tmp_tab[j]);
                    j++;
                }
                if ($scope.add == false) {
                    $scope.fillMinAndMax(tmp_tab);
                }
                $scope.data_avg.push($scope.makeAverage(tmp_tab));
                i++;
            }
            if ($scope.add == false) {
                $scope.nbr_mesure = total_tab.length;
                if (total_tab.length == 0) {
                    $scope.avg_tmp = "--";
                    $scope.last_tmp = "--";
                } else {
                    $scope.last_tmp = total_tab[total_tab.length - 1];
                    $scope.avg_tmp = Math.round($scope.makeAverage(total_tab) * 100) / 100;
                }
            }
            $scope.fillIndicatorAndPush();
        };

        $scope.sortAverages = function(allDatas) {
            $scope.channelSort(allDatas, $scope.makeAveragesMinMax);
        };

        //CORE
        $scope.sort = function(allDatas) {
            if ($scope.period === 'jour') {
                $scope.sortValues(allDatas);
            } else {
                $scope.sortAverages(allDatas);
            }
        };

        $scope.redirection = function(user) {
            $scope.noData = false;
            $scope.requestForAllValues(user, $scope.sort);
        };

        $scope.refresh = function() {
            $scope.allUsers = [];
            $scope.isUserDisplayed = [];
            $scope.data_tmp = [];
            $scope.labels_tmp = [];
            $scope.series_tmp = [];
            $scope.indicator_tab_tmp = [];
            var newDate = new Date().toISOString().split('T')[0];
            $scope.currentDate = newDate.split('-')[2] + '-' + newDate.split('-')[1] + '-' + newDate.split('-')[0];
            $scope.currentMonth = newDate.split('-')[1];
            $scope.currentYear = newDate.split('-')[0];
            $scope.requestForUsers();
            $scope.redirection($scope.username);
        };

        window.onload = $scope.refresh();
  }]);